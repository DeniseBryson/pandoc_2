# OSEG DIN Guideline - Documentation Example - Pandoc - 2 - `pdsite`

Result:
[HTML](https://osegtechdoctemplates.gitlab.io/pandoc_2/)

This contains just a [pdsite](https://github.com/hoijui/pdsite) based,
HTML only site.

* [x] Markdown sources
* [ ] PP (pre-processor) directives
* [ ] jekyll
* [x] pdsite
* [ ] sphinx
* [x] output: HTML
* [ ] output: PDF
